package com.Itau.cliente.repository;

import java.util.Optional;


import org.springframework.data.repository.CrudRepository;
import com.Itau.cliente.model.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, Integer>{
	Optional<Cliente> findById(int id);

}
