package com.Itau.cliente.controller;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.Itau.cliente.controller.ClienteController;
import com.Itau.cliente.model.Cliente;
import com.Itau.cliente.repository.ClienteRepository;

import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@WebMvcTest(ClienteController.class)
@AutoConfigureRestDocs(outputDir = "target/snippets")
public class ClienteControllerTest {
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ClienteRepository clienteRepository;
	
	@Test
	public void testaCliente() throws Exception{
		Cliente cliente = new Cliente();
		given(clienteRepository.save(cliente)).willReturn(cliente);
		//	    	this.mockMvc.perform(get("/cliente")).andExpect(status().isNotFound()).andDo(document("consultaCliente"));
		this.mockMvc.perform(get("/cliente")).andExpect(status().isOk()).andDo(document("consultaCliente"));
	}
}
